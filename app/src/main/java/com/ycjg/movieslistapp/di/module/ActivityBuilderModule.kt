package com.ycjg.movieslistapp.di.module

import com.ycjg.movieslistapp.di.annotations.ActivityScope
import com.ycjg.movieslistapp.views.home.HomeActivity
import com.ycjg.movieslistapp.views.home.HomeActivityModule
import com.ycjg.movieslistapp.views.login.LoginActivity
import com.ycjg.movieslistapp.views.login.LoginActivityModule
import com.ycjg.movieslistapp.views.movie_details.MovieDetailActivity
import com.ycjg.movieslistapp.views.movie_details.MovieDetailActivityModule
import com.ycjg.movieslistapp.views.movies.MoviesFragment
import com.ycjg.movieslistapp.views.movies.MoviesFragmentModule
import com.ycjg.movieslistapp.views.search.SearchFragment
import com.ycjg.movieslistapp.views.search.SearchFragmentModule
import com.ycjg.movieslistapp.views.watchlist.WatchlistFragment
import com.ycjg.movieslistapp.views.watchlist.WatchlistFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@Module
abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun bindLoginActivity() : LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [HomeActivityModule::class])
    abstract fun bindHomeActivity() : HomeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MoviesFragmentModule::class])
    abstract fun bindMoviesFragment() : MoviesFragment

    @ActivityScope
    @ContributesAndroidInjector(modules = [SearchFragmentModule::class])
    abstract fun bindSearchFragment() : SearchFragment

    @ActivityScope
    @ContributesAndroidInjector(modules = [WatchlistFragmentModule::class])
    abstract fun bindWatchlistFragment() : WatchlistFragment

    @ActivityScope
    @ContributesAndroidInjector(modules = [MovieDetailActivityModule::class])
    abstract fun bindMovieDetailActivity() : MovieDetailActivity
}