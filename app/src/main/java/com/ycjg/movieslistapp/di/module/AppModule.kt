package com.ycjg.movieslistapp.di.module

import android.content.Context
import com.ycjg.movieslistapp.MoviesApp
import com.ycjg.movieslistapp.data.account_manager.AppAccountManagerHelper
import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.di.annotations.ApplicationScope
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@Module(includes = [NetworkServiceModule::class])
class AppModule {

    @Provides
    @ApplicationScope
    fun provideContext(application : MoviesApp) : Context = application

    @Provides
    fun provideSchedulerProvider() = SchedulerProvider()

    @Provides
    fun providePreferences(mContext: Context) : PreferencesProvider = PreferencesProvider(mContext)

    @Provides
    fun provideAccountManagerHelper(mContext: Context) : AppAccountManagerHelper = AppAccountManagerHelper(mContext)
}