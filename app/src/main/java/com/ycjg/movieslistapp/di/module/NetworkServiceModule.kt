package com.ycjg.movieslistapp.di.module

import com.ycjg.movieslistapp.data.MovieService
import com.ycjg.movieslistapp.di.annotations.ApplicationScope
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@Module(includes = [NetworkModule::class])
class NetworkServiceModule {

    @Provides
    @ApplicationScope
    fun provideMovieService(retrofit: Retrofit) : MovieService {
        return retrofit.create(MovieService::class.java)
    }
}