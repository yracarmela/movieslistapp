package com.ycjg.movieslistapp.di.component

import com.ycjg.movieslistapp.MoviesApp
import com.ycjg.movieslistapp.di.annotations.ApplicationScope
import com.ycjg.movieslistapp.di.module.ActivityBuilderModule
import com.ycjg.movieslistapp.di.module.AppModule
import com.ycjg.movieslistapp.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@ApplicationScope
@Component(modules = arrayOf(AndroidSupportInjectionModule::class,
    AppModule::class,
    ViewModelModule::class,
    ActivityBuilderModule::class))
interface ApplicationComponent : AndroidInjector<MoviesApp> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MoviesApp>()
}