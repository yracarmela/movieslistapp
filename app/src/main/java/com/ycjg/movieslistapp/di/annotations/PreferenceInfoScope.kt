package com.ycjg.movieslistapp.di.annotations

import javax.inject.Scope

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class PreferenceInfoScope