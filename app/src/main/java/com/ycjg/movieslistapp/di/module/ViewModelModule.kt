package com.ycjg.movieslistapp.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.ycjg.movieslistapp.common.ViewModelFactory
import com.ycjg.movieslistapp.di.annotations.ViewModelKey
import com.ycjg.movieslistapp.views.home.HomeViewModel
import com.ycjg.movieslistapp.views.login.LoginViewModel
import com.ycjg.movieslistapp.views.movie_details.MovieDetailViewModel
import com.ycjg.movieslistapp.views.movies.MoviesViewModel
import com.ycjg.movieslistapp.views.search.SearchViewModel
import com.ycjg.movieslistapp.views.watchlist.WatchlistViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homeViewModel: HomeViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel::class)
    abstract fun bindMoviesViewModel(moviesViewModel: MoviesViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WatchlistViewModel::class)
    abstract fun bindWatchlistViewModel(watchlistViewModel: WatchlistViewModel) : ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MovieDetailViewModel::class)
    abstract fun bindMovieDetailViewModel(movieDetailViewModel: MovieDetailViewModel) : ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory) : ViewModelProvider.Factory
}