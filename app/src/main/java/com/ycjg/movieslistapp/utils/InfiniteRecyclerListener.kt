package com.ycjg.movieslistapp.utils

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
abstract class InfiniteRecyclerListener :
    RecyclerView.OnScrollListener() {

    private val visibleThreshold = 2
    private var lastVisibleItem: Int = 0
    private var totalItemCount:Int = 0
    private var loading: Boolean = false
    private var currentPage: Int = 1
    private var previousTotal = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val manager = recyclerView.layoutManager
        if (manager is GridLayoutManager) {
            val gridLayoutManager: GridLayoutManager? = manager
            totalItemCount = gridLayoutManager!!.itemCount
            lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition()
        } else if (manager is LinearLayoutManager) {
            val linearLayoutManager: LinearLayoutManager? = manager
            totalItemCount = linearLayoutManager!!.itemCount
            lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition()
        }

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }

        if (!loading && totalItemCount <= lastVisibleItem + visibleThreshold) {
            //End has been reached
            currentPage++
            onLoadMore(currentPage)
            loading = true
        }
    }

    fun downloadComplete() {
        loading = false
    }

    abstract fun onLoadMore(currentPage: Int)
}