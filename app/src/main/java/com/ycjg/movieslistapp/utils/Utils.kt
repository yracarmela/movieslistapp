package com.ycjg.movieslistapp.utils

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.net.Uri
import android.util.Patterns
import android.util.TypedValue
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by yragalvez on 26/02/2019.
 * MoviesListApp
 */
class Utils {

    companion object {

        fun isValidEmail(email : String) : Boolean {
            val emailPattern = Patterns.EMAIL_ADDRESS.pattern()
            var strEmail : String = email
            if (email.isEmpty()) strEmail = ""
            return strEmail.trim { it <= ' ' }.matches(emailPattern.toRegex())
        }

        fun isLengthValid(password : String) : Boolean {
            return password.length >= 4
        }

        fun getAuthenticationUrl(requestToken: String?) : String {
            return "https://www.themoviedb.org/authenticate/$requestToken"
        }

        fun loadExternalBrowser(activity: Activity, url: String?) {
            val uriUrl = Uri.parse(url)
            val loadBrowser = Intent()
            loadBrowser.action = Intent.ACTION_VIEW
            loadBrowser.addCategory(Intent.CATEGORY_BROWSABLE)
            loadBrowser.data = uriUrl
            activity.startActivity(loadBrowser)
        }

        fun dpToPx(dp: Int, r: Resources): Int {
            return Math.round(
                TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dp.toFloat(),
                    r.displayMetrics
                )
            )
        }

        fun formatDate(date: Date, format: String?): String {
            val mFormat = SimpleDateFormat(format, Locale.getDefault())
            return mFormat.format(date.time)
        }
    }
}