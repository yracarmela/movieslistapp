package com.ycjg.movieslistapp.utils

import com.ycjg.movieslistapp.BuildConfig

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
object AppConstants {
    const val BASE_URL = BuildConfig.BASE_URL
    const val API_KEY = BuildConfig.API_KEY
    const val ACCOUNT_TYPE = BuildConfig.ACCOUNT_TYPE
    const val GENERAL_KEY_TYPE = "User"
    const val USER_INFO_KEY = "UserInfo"
    const val PREFS_NAME = "MoviesPrefs"
    const val EXITING_TAG = "is_exiting"
    const val IMAGE_URL_POSTER = "${BuildConfig.IMAGE_URL_LIST}/w185"
    const val IMAGE_URL_BACKDROP = "${BuildConfig.IMAGE_URL_LIST}/w780"

    const val SUCCESS_STATUS = 1
    const val SUCCESS_UPDATE = 12

    const val DEFAULT_STRING = ""
    const val DEFAULT_INT = -1
    const val DEFAULT_LONG = -1L
    const val DEFAULT_BOOLEAN = false
}