package com.ycjg.movieslistapp.views.watchlist

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.models.list.MovieWatchlist
import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import com.ycjg.movieslistapp.utils.AppConstants
import javax.inject.Inject

class WatchlistViewModel @Inject constructor(private val mMovieRepository: MovieRepository,
                                             private val schedulerProvider: SchedulerProvider,
                                             private val mPrefsProvider: PreferencesProvider): BaseViewModel() {

    private val TAG = WatchlistViewModel::class.java.simpleName
    private val watchlist = MutableLiveData<MovieWatchlist>()

    fun loadWatchlist(page: Int) {
        val accountId = mPrefsProvider.getAccountId()
        val sessionId = mPrefsProvider.getSessionId()

        compositeDisposable.add(
            mMovieRepository.getWatchlist(accountId!!, AppConstants.API_KEY, sessionId!!, page)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(this::getWatchlist) { th: Throwable? -> onErrorResult(TAG, "loadWatchlist()", th)})
    }

    private fun getWatchlist(movieWatchlist: MovieWatchlist) {
        watchlist.value = movieWatchlist
    }

    fun getMovieWatchlist(): LiveData<MovieWatchlist> {
        return watchlist
    }

}
