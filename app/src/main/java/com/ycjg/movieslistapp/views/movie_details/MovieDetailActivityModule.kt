package com.ycjg.movieslistapp.views.movie_details

import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
@Module
class MovieDetailActivityModule {

    @Provides
    fun provideViewModel(movieRepository: MovieRepository, schedulerProvider: SchedulerProvider, mPrefsProvider: PreferencesProvider)
            = MovieDetailViewModel(movieRepository, schedulerProvider, mPrefsProvider)
}