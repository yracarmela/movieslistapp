package com.ycjg.movieslistapp.views.watchlist

import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
@Module
class WatchlistFragmentModule {

    @Provides
    fun provideModel(mMovieRepository: MovieRepository, schedulerProvider: SchedulerProvider,
                     mPrefsProvider: PreferencesProvider)
            = WatchlistViewModel(mMovieRepository, schedulerProvider, mPrefsProvider)
}