package com.ycjg.movieslistapp.views.home

import com.ycjg.movieslistapp.data.account_manager.AppAccountManagerHelper
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
@Module
class HomeActivityModule {

    @Provides
    fun provideViewModel(mManagerHelper: AppAccountManagerHelper) = HomeViewModel(mManagerHelper)
}