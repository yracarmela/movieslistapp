package com.ycjg.movieslistapp.views.watchlist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseFragment
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.data.models.list.MovieWatchlist
import com.ycjg.movieslistapp.utils.GridSpacingItemDecoration
import com.ycjg.movieslistapp.utils.InfiniteRecyclerListener
import com.ycjg.movieslistapp.utils.Utils
import com.ycjg.movieslistapp.views.movie_details.MovieDetailActivity
import com.ycjg.movieslistapp.views.movies.adapter.MoviesAdapter
import kotlinx.android.synthetic.main.movies_fragment.*
import javax.inject.Inject

class WatchlistFragment : BaseFragment(), MoviesAdapter.OnItemClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var watchlistViewModel: WatchlistViewModel
    private lateinit var mMoviesAdapter: MoviesAdapter
    private var movieWatchlist: MovieWatchlist? = null

    companion object {
        fun newInstance() = WatchlistFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.watchlist_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        watchlistViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(WatchlistViewModel::class.java)

        setUp()
    }

    override fun onItemClick(movie: MovieDetails) {
        val intent: Intent = MovieDetailActivity.getStartIntent(context!!)
        intent.putExtra(MovieDetailActivity.MOVIE_ID, movie.movieId)
        startActivity(intent)
    }

    override fun setUp() {
        setupWatchlist()
        showProgress(true)
        watchlistViewModel.loadWatchlist(1)
        watchlistViewModel.getMovieWatchlist().observe(this, Observer { updateMovieList(it) })
    }

    private fun setupWatchlist() {
        val layoutManager = GridLayoutManager(activity, 3)
        rvMovies.layoutManager = layoutManager
        rvMovies.addItemDecoration(GridSpacingItemDecoration(3, Utils.dpToPx(8, resources), true))
        rvMovies.itemAnimator = DefaultItemAnimator()
        rvMovies.addOnScrollListener(object: InfiniteRecyclerListener() {
            override fun onLoadMore(currentPage: Int) {
                loadMoreMovies(currentPage)
            }
        })

        mMoviesAdapter = MoviesAdapter(activity!!)
        mMoviesAdapter.setItemClickListener(this)
        mMoviesAdapter.setHasStableIds(true)

        rvMovies.adapter = mMoviesAdapter
    }

    private fun updateMovieList(mMovieList: MovieWatchlist?) {
        showProgress(false)
        movieWatchlist = mMovieList
        mMoviesAdapter.setItems(mMovieList?.results!!)
        mMoviesAdapter.notifyDataSetChanged()
        mMoviesAdapter.showLoading(false)
    }

    private fun loadMoreMovies(page: Int) {
        val hasNextPage = movieWatchlist?.totalPages!! > movieWatchlist?.page!!

        mMoviesAdapter.showLoading(hasNextPage)
        if (hasNextPage) watchlistViewModel.loadWatchlist(page)
    }

    private fun showProgress(showProgress: Boolean?) {
        movie_progress.visibility = if (showProgress!!) View.VISIBLE else View.GONE
    }
}
