package com.ycjg.movieslistapp.views.search.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import com.bumptech.glide.Glide
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.utils.Utils
import com.ycjg.movieslistapp.views.movies.adapter.FooterLoaderAdapter
import kotlinx.android.synthetic.main.layout_search_item.view.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
class SearchAdapter constructor(private val mActivity: Activity) :
    FooterLoaderAdapter<MovieDetails>(mActivity) {

    private lateinit var mItemClickListener: OnItemClickListener

    override fun getCustomItemId(position: Int): Long {
        return mItems?.get(position)?.movieId!!
    }

    override fun getItemViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MovieViewHolder(mInflater.inflate(R.layout.layout_search_item, parent, false))
    }

    override fun bindCustomViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            val movie = mItems?.get(holder.adapterPosition)

            val imageUrl = "${AppConstants.IMAGE_URL_POSTER}/${movie?.posterPath}"
            Glide.with(mActivity)
                .load(imageUrl)
                .into(holder.itemView.ivMovie)

            holder.itemView.tvMovieTitle.text = movie?.movieTitle
            holder.itemView.tvOverview.text = movie?.overview
            if (movie?.releaseDate != null) {
                val releaseDate = Date(movie.releaseDate?.time!!)
                holder.itemView.tvReleaseDate.text = Utils.formatDate(releaseDate, "MMM dd, yyyy")
            }
            holder.itemView.search_item.setOnTouchListener(object: View.OnTouchListener {
                @SuppressLint("ClickableViewAccessibility")
                override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                    when (p1?.action) {
                        MotionEvent.ACTION_UP -> movie?.let { mItemClickListener.onItemClick(it) }
                    }
                    return p0?.onTouchEvent(p1)?: true
                }

            })
        }
    }

    fun setItemClickListener(itemClickListener: OnItemClickListener) {
        mItemClickListener = itemClickListener
    }

    inner class MovieViewHolder internal constructor(rootView: View) :
        RecyclerView.ViewHolder(rootView)


    interface OnItemClickListener {
        fun onItemClick(movie: MovieDetails)
    }

    class SearchFilter constructor(private var mMovieList: MutableList<MovieDetails>,
                                   private var searchAdapter: SearchAdapter) : Filter() {

        override fun performFiltering(p0: CharSequence?): FilterResults {
            val filterResults = FilterResults()
            if (p0 != null && p0.isNotEmpty()) {
                val filterList = ArrayList<MovieDetails>(filterByMovieTitle(p0.toString()))
                filterResults.count = filterList.size
                filterResults.values = filterList
            } else {
                filterResults.count = mMovieList.size
                filterResults.values = mMovieList
            }

            return filterResults
        }

        private fun filterByMovieTitle(queryString: String): ArrayList<MovieDetails> {
            val filteredList = ArrayList<MovieDetails>()
            for (movieDetails: MovieDetails in mMovieList) {
                if (movieDetails.movieTitle.contains(queryString) ||
                        movieDetails.originalTitle?.contains(queryString)!!)
                    filteredList.add(movieDetails)
            }

            return filteredList
        }

        override fun publishResults(p0: CharSequence?, p1: FilterResults?) {
            searchAdapter.mItems = p1?.values as ArrayList<MovieDetails>
            searchAdapter.notifyDataSetChanged()
        }
    }
}