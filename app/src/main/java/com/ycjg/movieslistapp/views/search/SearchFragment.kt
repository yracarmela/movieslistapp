package com.ycjg.movieslistapp.views.search

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseFragment
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.utils.InfiniteRecyclerListener
import com.ycjg.movieslistapp.views.movie_details.MovieDetailActivity
import com.ycjg.movieslistapp.views.search.adapter.SearchAdapter
import kotlinx.android.synthetic.main.search_fragment.*
import javax.inject.Inject

class SearchFragment : BaseFragment(), SearchAdapter.OnItemClickListener {


    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var searchViewModel: SearchViewModel
    private lateinit var searchAdapter: SearchAdapter
    private var searchString = ""

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.search_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        searchViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(SearchViewModel::class.java)

        setUp()
    }

    override fun onItemClick(movie: MovieDetails) {
        val intent: Intent = MovieDetailActivity.getStartIntent(context!!)
        intent.putExtra(MovieDetailActivity.MOVIE_ID, movie.movieId)
        startActivity(intent)
    }

    override fun setUp() {
        addListeners()
        setupSearchResult()
        searchViewModel.getSearchResultList().observe(this, Observer { updateSearchResultList(it) })
        searchViewModel.showLoading().observe(this, Observer { showProgress(it) })
    }

    private fun setupSearchResult() {
        val layoutManager = LinearLayoutManager(context)
        rvSearchResult.layoutManager = layoutManager
        rvSearchResult.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        rvSearchResult.itemAnimator = DefaultItemAnimator()
        rvSearchResult.addOnScrollListener(object: InfiniteRecyclerListener() {
            override fun onLoadMore(currentPage: Int) {
                searchForMovie(searchString, currentPage)
            }
        })
        searchAdapter = SearchAdapter(activity!!)
        searchAdapter.setItemClickListener(this)
        searchAdapter.setHasStableIds(true)

        rvSearchResult.adapter = searchAdapter
    }

    private fun addListeners() {
        svSearch.isActivated = true
        svSearch.onActionViewExpanded()
        svSearch.isIconified = false
        svSearch.queryHint = getString(R.string.search_movies_hint)

        svSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(s: String): Boolean {
                searchAdapter.clearItems()
                searchAdapter.notifyDataSetChanged()
                searchString = s
                searchForMovie(s, 1)
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                return false
            }
        })
        svSearch.findViewById<View>(R.id.search_close_btn)
            .setOnClickListener { clearQuery() }
    }

    private fun clearQuery() {
        searchAdapter.clearItems()
        searchAdapter.notifyDataSetChanged()
        svSearch.setQuery("", false)
        search_progress.visibility = View.GONE
        rvSearchResult.visibility = View.GONE
    }

    private fun searchForMovie(s: String, page: Int) {
        searchViewModel.getSearchResult(s, page)
    }

    private fun showProgress(showProgress: Boolean?) {
        search_progress.visibility = if (showProgress!!) View.VISIBLE else View.GONE
        rvSearchResult.visibility = if (showProgress) View.GONE else View.VISIBLE
    }

    private fun updateSearchResultList(mMovieList: List<MovieDetails>?) {
        searchAdapter.setItems(mMovieList!!)
        searchAdapter.notifyDataSetChanged()
    }
}
