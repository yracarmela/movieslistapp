package com.ycjg.movieslistapp.views.search

import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
@Module
class SearchFragmentModule {

    @Provides
    fun provideModel(movieRepository: MovieRepository, schedulerProvider: SchedulerProvider)
            = SearchViewModel(movieRepository, schedulerProvider)
}