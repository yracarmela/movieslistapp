package com.ycjg.movieslistapp.views.movies.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ycjg.movieslistapp.R

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
abstract class FooterLoaderAdapter<T>(context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var showLoader: Boolean = false

    var mItems: MutableList<T>? = ArrayList()
    var mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == VIEWTYPE_LOADER) {
            val view = mInflater.inflate(R.layout.item_loader_layout, parent, false)
            return LoaderViewHolder(view)
        } else if (viewType == VIEWTYPE_ITEM)
            return getItemViewHolder(parent)

        throw IllegalArgumentException("Invalid ViewType: $viewType")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LoaderViewHolder) {
            holder.mProgressBar?.visibility = if (showLoader) View.VISIBLE else View.GONE
            return
        }

        bindCustomViewHolder(holder, position)
    }

    override fun getItemCount(): Int {
        return if (mItems == null || mItems!!.isEmpty()) 0 else mItems!!.size + 1

        // +1 for loader
    }


    override fun getItemId(position: Int): Long {
        // loader can't be at position 0
        // loader can only be at the last position
        return if (position != 0 && position == itemCount - 1) {

            // id of loader is considered as -1 here
            -1
        } else getCustomItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return if (position != 0 && position == itemCount - 1) VIEWTYPE_LOADER else VIEWTYPE_ITEM

    }

    fun showLoading(status: Boolean) {
        showLoader = status
    }

    fun setItems(items: List<T>) {
        mItems?.addAll(items)
    }

    fun clearItems() {
        mItems = ArrayList()
    }

    abstract fun getCustomItemId(position: Int): Long
    abstract fun getItemViewHolder(parent: ViewGroup): RecyclerView.ViewHolder
    abstract fun bindCustomViewHolder(holder: RecyclerView.ViewHolder, position: Int)

    companion object {
        private const val VIEWTYPE_ITEM = 1
        private const val VIEWTYPE_LOADER = 2
    }
}