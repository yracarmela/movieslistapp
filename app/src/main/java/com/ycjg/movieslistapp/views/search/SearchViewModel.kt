package com.ycjg.movieslistapp.views.search

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.data.models.list.MovieResponseList
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import com.ycjg.movieslistapp.utils.AppConstants
import javax.inject.Inject

class SearchViewModel @Inject constructor(private var mMovieRepo: MovieRepository,
                                          private var schedulerProvider: SchedulerProvider): BaseViewModel() {

    private val TAG = SearchViewModel::class.java.simpleName
    private val mFilteredMovies = MutableLiveData<List<MovieDetails>>()
    private val isLoading = MutableLiveData<Boolean>()

    fun getSearchResult(queryString: String, page: Int) {
        isLoading.value = true
        compositeDisposable.add(
            mMovieRepo.getSearchResult(AppConstants.API_KEY, queryString, page)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe( {responseList: MovieResponseList? ->
                    isLoading.value = false
                    mFilteredMovies.value = responseList?.results
                }) { th: Throwable? -> onErrorResult(TAG, "getSearchResult()", th)})
    }

    fun getSearchResultList(): LiveData<List<MovieDetails>> {
        return mFilteredMovies
    }

    fun showLoading(): LiveData<Boolean> {
        return isLoading
    }
}
