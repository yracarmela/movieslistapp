package com.ycjg.movieslistapp.views.movie_details

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseActivity
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.data.models.MovieReviews
import com.ycjg.movieslistapp.data.models.StatusResponse
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.utils.Utils
import com.ycjg.movieslistapp.views.home.HomeActivity
import com.ycjg.movieslistapp.views.movie_details.adapter.ReviewsAdapter
import kotlinx.android.synthetic.main.activity_movie_detail.*
import kotlinx.android.synthetic.main.layout_movie_details.*
import java.util.*
import javax.inject.Inject

class MovieDetailActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var movieDetailViewModel: MovieDetailViewModel
    private var movieId: Long? = 0

    private lateinit var mReviewsAdapter: ReviewsAdapter

    companion object {
        const val MOVIE_ID = "MOVIE_ID"

        fun getStartIntent(context: Context): Intent {
            return Intent(context, MovieDetailActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        movieDetailViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MovieDetailViewModel::class.java)

        setUp()
    }

    override fun setUp() {
        val bundle: Bundle? = intent?.extras
        movieId = bundle?.getLong(MOVIE_ID)

        addListeners()

        movieDetailViewModel.getMovieDetails(movieId)
        movieDetailViewModel.getMovie().observe(this, Observer { setMovieDetails(it) })
        movieDetailViewModel.getMovieReviews(movieId)
        movieDetailViewModel.getReviews().observe(this, Observer { setMovieReviews(it) })
        movieDetailViewModel.getRatingStatus().observe(this, Observer { checkRatingStatus(it) })
        movieDetailViewModel.getRemovedRatingStatus().observe(this, Observer { checkRemovedRatingStatus(it) })
        movieDetailViewModel.getWatchlistStatus().observe(this, Observer { checkWatchlistStatus(it) })
    }

    private fun addListeners() {
        fab_add.setOnClickListener { movieDetailViewModel.addMovieToWatchlist(movieId!!) }
        ratingBar.setOnRatingBarChangeListener { _, fl, _ ->
            val rating: Float = fl * 2
            if (rating > 0F)
                movieDetailViewModel.rateMovie(movieId, rating.toInt())
            else
                movieDetailViewModel.removeRating(movieId)
        }
    }


    private fun setMovieDetails(movieDetails: MovieDetails?) {
        toolbar_layout.title = movieDetails?.movieTitle

        val movieBackdrop = "${AppConstants.IMAGE_URL_BACKDROP}/${movieDetails?.backdropPath}"
        Glide.with(this)
            .load(movieBackdrop)
            .into(ivMovie)

        tvTagline.text = movieDetails?.movieTagline

        val movieGenre = movieDetails?.genre?.joinToString { it.genreName }
        tvGenreValue.text = movieGenre

        tvTitle.text = movieDetails?.originalTitle

        if (movieDetails?.releaseDate != null) {
            val releaseDate = Date(movieDetails.releaseDate?.time!!)
            tvReleaseDate.text = Utils.formatDate(releaseDate, "MMM dd, yyyy")
        } else tvReleaseDate.text = ""

        val sRuntime = "${movieDetails?.runtime} minutes"
        tvRuntime.text = sRuntime

        tvStatus.text = movieDetails?.movieStatus

        tvOverview.text = movieDetails?.overview

        val sVotes = "${movieDetails?.voteAverage}/10"
        tvVotes.text = sVotes

        tvVoteCount.text = "${movieDetails?.voteCount}"
    }

    private fun setMovieReviews(movieReviews: List<MovieReviews>?) {
        cvReviews.visibility = if (movieReviews!!.isNotEmpty()) View.VISIBLE else View.GONE
        mReviewsAdapter = ReviewsAdapter(movieReviews)
        expandableReviews.setAdapter(mReviewsAdapter)
    }

    private fun checkRatingStatus(statusResponse: StatusResponse?) {
        val statusCode = statusResponse?.statusCode
        if (AppConstants.SUCCESS_STATUS == statusCode ||
                AppConstants.SUCCESS_UPDATE == statusCode)
            showMessage("Successfully rated")
    }

    private fun checkRemovedRatingStatus(statusResponse: StatusResponse?) {
        if (AppConstants.SUCCESS_STATUS == statusResponse?.statusCode)
            showMessage("Removed rating")
    }

    private fun checkWatchlistStatus(statusResponse: StatusResponse?) {
        if (AppConstants.SUCCESS_STATUS == statusResponse?.statusCode)
            showMessage("Movie added to Watchlist")
    }

    override fun onBackPressed() {
        val upIntent: Intent = HomeActivity.getStartIntent(this)
        navigateUpTo(upIntent)

        super.onBackPressed()
    }
}
