package com.ycjg.movieslistapp.views.movies.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.utils.AppConstants
import kotlinx.android.synthetic.main.movie_layout_item.view.*

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
class MoviesAdapter constructor(private val mActivity: Activity) :
    FooterLoaderAdapter<MovieDetails>(mActivity) {

    private lateinit var mItemClickListener: OnItemClickListener

    override fun getCustomItemId(position: Int): Long {
        return mItems?.get(position)?.movieId!!
    }

    override fun getItemViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        return MovieViewHolder(mInflater.inflate(R.layout.movie_layout_item, parent, false))
    }

    override fun bindCustomViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MovieViewHolder) {
            val movie = mItems?.get(holder.adapterPosition)
            val imageUrl = "${AppConstants.IMAGE_URL_POSTER}/${movie?.posterPath}"

            Glide.with(mActivity)
                .load(imageUrl)
                .into(holder.itemView.ivMovie)

            holder.itemView.movie_item.setOnTouchListener(object: View.OnTouchListener {
                @SuppressLint("ClickableViewAccessibility")
                override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                    when (p1?.action) {
                        MotionEvent.ACTION_UP -> mItemClickListener.onItemClick(movie!!)
                    }
                    return p0?.onTouchEvent(p1)?: true
                }
            })
        }
    }

    fun setItemClickListener(itemClickListener: OnItemClickListener) {
        mItemClickListener = itemClickListener
    }

    inner class MovieViewHolder internal constructor(rootView: View) :
        RecyclerView.ViewHolder(rootView)

    interface OnItemClickListener {
        fun onItemClick(movie: MovieDetails)
    }
}