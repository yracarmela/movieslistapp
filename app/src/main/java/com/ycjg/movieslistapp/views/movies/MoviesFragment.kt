package com.ycjg.movieslistapp.views.movies

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseFragment
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.utils.GridSpacingItemDecoration
import com.ycjg.movieslistapp.utils.InfiniteRecyclerListener
import com.ycjg.movieslistapp.utils.Utils
import com.ycjg.movieslistapp.views.movie_details.MovieDetailActivity
import com.ycjg.movieslistapp.views.movies.adapter.MoviesAdapter
import kotlinx.android.synthetic.main.movies_fragment.*
import javax.inject.Inject

class MoviesFragment : BaseFragment(), MoviesAdapter.OnItemClickListener {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var mMoviesAdapter: MoviesAdapter

    companion object {
        fun newInstance() = MoviesFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.movies_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        moviesViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(MoviesViewModel::class.java)

        setUp()
    }

    override fun onItemClick(movie: MovieDetails) {
        val intent: Intent = MovieDetailActivity.getStartIntent(context!!)
        intent.putExtra(MovieDetailActivity.MOVIE_ID, movie.movieId)
        startActivity(intent)
    }

    override fun setUp() {
        setupMovieList()
        showProgress(true)
        moviesViewModel.loadTrendingMovies(1)
        moviesViewModel.getMovieList().observe(this, Observer { updateMovieList(it) })
    }

    private fun showProgress(showProgress: Boolean?) {
        movie_progress.visibility = if (showProgress!!) View.VISIBLE else View.GONE
        tvTrendingMovies.visibility = if (showProgress) View.GONE else View.VISIBLE
    }

    private fun setupMovieList() {
        val layoutManager = GridLayoutManager(activity, 3)
        rvMovies.layoutManager = layoutManager
        rvMovies.addItemDecoration(GridSpacingItemDecoration(3, Utils.dpToPx(8, resources), true))
        rvMovies.itemAnimator = DefaultItemAnimator()
        rvMovies.addOnScrollListener(object: InfiniteRecyclerListener() {
            override fun onLoadMore(currentPage: Int) {
                loadMoreMovies(currentPage)
            }
        })

        mMoviesAdapter = MoviesAdapter(activity!!)
        mMoviesAdapter.setItemClickListener(this)
        mMoviesAdapter.setHasStableIds(true)

        rvMovies.adapter = mMoviesAdapter
    }

    private fun updateMovieList(mMovieList: List<MovieDetails>?) {
        showProgress(false)
        mMoviesAdapter.setItems(mMovieList!!)
        mMoviesAdapter.notifyDataSetChanged()
        mMoviesAdapter.showLoading(false)
    }

    private fun loadMoreMovies(page: Int) {
        mMoviesAdapter.showLoading(true)
        moviesViewModel.loadTrendingMovies(page)
    }
}
