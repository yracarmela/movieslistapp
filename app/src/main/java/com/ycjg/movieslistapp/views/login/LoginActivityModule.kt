package com.ycjg.movieslistapp.views.login

import com.ycjg.movieslistapp.data.account_manager.AppAccountManagerHelper
import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 25/02/2019.
 * MoviesListApp
 */
@Module
class LoginActivityModule {

    @Provides
    fun provideViewModel(movieRepository: MovieRepository, schedulerProvider: SchedulerProvider,
                         preferencesProvider: PreferencesProvider, mAccountManager: AppAccountManagerHelper)
            = LoginViewModel(movieRepository, schedulerProvider, preferencesProvider, mAccountManager)
}