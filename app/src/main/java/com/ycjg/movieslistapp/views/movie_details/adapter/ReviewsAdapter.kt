package com.ycjg.movieslistapp.views.movie_details.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.data.models.MovieReviews

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
class ReviewsAdapter constructor(movieReviews: List<MovieReviews>) : BaseExpandableListAdapter() {

    private var mMovieReviews: MutableList<MovieReviews> = ArrayList(movieReviews)

    override fun getGroup(p0: Int): Any {
        return mMovieReviews[p0]
    }

    override fun isChildSelectable(p0: Int, p1: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(p0: Int, p1: Boolean, p2: View?, p3: ViewGroup?): View {
        var convertView = p2
        val listTitle = getGroup(p0) as MovieReviews
        if (convertView == null) {
            val layoutInflater = p3?.context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.layout_review_heading, p3, false)
        }
        val listTitleTextView = convertView
            ?.findViewById(R.id.tvTitle) as TextView
        listTitleTextView.setTypeface(null, Typeface.BOLD)
        listTitleTextView.text = listTitle.author
        return convertView
    }

    override fun getChildrenCount(p0: Int): Int {
        return mMovieReviews.size
    }

    override fun getChild(p0: Int, p1: Int): Any {
        return mMovieReviews[p1]
    }

    override fun getGroupId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getChildView(p0: Int, p1: Int, p2: Boolean, p3: View?, p4: ViewGroup?): View {
        val expandedListText = getChild(p0, p1) as MovieReviews
        var convertView: View? = p3
        if (convertView == null) {
            val layoutInflater = p4?.context
                ?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.layout_review_item, p4, false)
        }

        val expandedListTextView = convertView
            ?.findViewById(R.id.tvReviewItem) as TextView
        expandedListTextView.text = expandedListText.reviewContent
        return convertView
    }

    override fun getChildId(p0: Int, p1: Int): Long {
        return p0.toLong()
    }

    override fun getGroupCount(): Int {
        return mMovieReviews.size
    }
}