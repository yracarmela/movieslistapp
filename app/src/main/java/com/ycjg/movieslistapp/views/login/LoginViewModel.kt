package com.ycjg.movieslistapp.views.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.text.TextUtils
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.account_manager.AppAccountManagerHelper
import com.ycjg.movieslistapp.data.models.AccountDetails
import com.ycjg.movieslistapp.data.models.SessionResponse
import com.ycjg.movieslistapp.data.models.TokenResponse
import com.ycjg.movieslistapp.data.models.User
import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.utils.Utils
import javax.inject.Inject

/**
 * Created by yragalvez on 25/02/2019.
 * MoviesListApp
 */
class LoginViewModel @Inject constructor(private val movieRepository: MovieRepository,
                                         private val schedulerProvider: SchedulerProvider,
                                         private val mPrefsHelper: PreferencesProvider,
                                         private val mAccountManager:  AppAccountManagerHelper) : BaseViewModel() {

    private val TAG = "LoginViewModel"

    private val emailErrorMsg = MutableLiveData<Int>()
    private val passwordErrorMsg = MutableLiveData<Int>()

    private val mSessionResponse = MutableLiveData<SessionResponse>()
    private val returnIntent = MutableLiveData<Intent>()

    private val isLoading = MutableLiveData<Boolean>()

    fun login(userId: String, password: String) {
        var isValid = true

        if (TextUtils.isEmpty(userId)) {
            emailErrorMsg.value = R.string.required_error
            isValid = false
        } else if (!Utils.isLengthValid(userId)) {
            emailErrorMsg.value = R.string.invalid_userid_error
            isValid = false
        }

        if (TextUtils.isEmpty(password)) {
            passwordErrorMsg.value = R.string.required_error
            isValid = false
        } else if (!Utils.isLengthValid(password)) {
            passwordErrorMsg.value = R.string.invalid_password_error
            isValid = false
        }

        if (isValid) createRequestToken(userId, password)
    }

    private fun createRequestToken(userId: String, password: String) {
        isLoading.value = true

        compositeDisposable.add(movieRepository.createRequestToken(AppConstants.API_KEY)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe({attemptLogin(userId, password, it.requestToken)}
            ) { th: Throwable? -> onErrorResult(TAG, "createRequestToken()", th)})
    }

    private fun attemptLogin(userId: String, password: String, requestToken: String) {
        compositeDisposable.add(
            movieRepository.validateLogin(AppConstants.API_KEY, userId, password, requestToken)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({t -> onLoginSuccess(t, userId, password, requestToken) }
                ) { th: Throwable? -> onErrorResult(TAG, "attemptLogin()", th)})
    }

    private fun onLoginSuccess(result: TokenResponse, userName: String, password: String, requestToken: String) {
        isLoading.value = false

        val user = User(userName, password, requestToken)
        returnIntent.value = mAccountManager.addAccount(user, password)
        mPrefsHelper.setCurrentUser(userName)
        createSession(requestToken)
        getAccountDetails()
    }

    private fun createSession(requestToken: String?) {
        compositeDisposable.add(movieRepository.createSessionId(AppConstants.API_KEY, requestToken!!)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe(this::onCreateSessionSuccess
            ) { th: Throwable? -> onErrorResult(TAG, "createSession()", th)})
    }

    private fun onCreateSessionSuccess(sessionResponse: SessionResponse) {
        mPrefsHelper.setSessionId(sessionResponse.sessionId)
        mSessionResponse.value = sessionResponse
    }

    private fun getAccountDetails() {
        val sessionId = mPrefsHelper.getSessionId()
        compositeDisposable.add(
            movieRepository.getAccountDetails(AppConstants.API_KEY, sessionId!!)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(this::onRetrieveAccountDetails)
                { th: Throwable? -> onErrorResult(TAG, "getAccountDetails()", th)})
    }

    private fun onRetrieveAccountDetails(accountDetails: AccountDetails) {
        mPrefsHelper.setAccountId(accountDetails.accountId)
        mPrefsHelper.setAccountName(accountDetails.accountName)
    }

    fun getEmailErrorMsg() : LiveData<Int> {
        return emailErrorMsg
    }

    fun getPasswordErrorMsg() : LiveData<Int> {
        return passwordErrorMsg
    }

    fun getLoading() : LiveData<Boolean> {
        return isLoading
    }

    fun getReturnIntent() : LiveData<Intent> {
        return returnIntent
    }
}