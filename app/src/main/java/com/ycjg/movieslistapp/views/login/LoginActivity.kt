package com.ycjg.movieslistapp.views.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseAuthActivity
import com.ycjg.movieslistapp.views.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseAuthActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var loginViewModel: LoginViewModel

    companion object {
        fun getStartIntent(context : Context) : Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onCreate(icicle: Bundle?) {
        super.onCreate(icicle)
        setContentView(R.layout.activity_login)

        loginViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(LoginViewModel::class.java)

        setUp()
        addListener()
    }

    override fun setUp() {
        loginViewModel.getLoading().observe(this, Observer { showProgress(it) })
    }

    private fun addListener() {
        etPassword.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        btnSignIn.setOnClickListener { attemptLogin() }
    }

    private fun setEmailError(errorMsg : Int?) {
        etUserName.error = resources.getString(errorMsg!!)
    }

    private fun setPasswordError(errorMsg: Int?) {
        etPassword.error = resources.getString(errorMsg!!)
    }

    private fun attemptLogin() {
        etUserName.error = null
        etPassword.error = null

        val userNameStr = etUserName.text.toString()
        val passwordStr = etPassword.text.toString()

        loginViewModel.getEmailErrorMsg().observe(this, Observer { setEmailError(it) })
        loginViewModel.getPasswordErrorMsg().observe(this, Observer { setPasswordError(it) })
        loginViewModel.login(userNameStr, passwordStr)

        loginViewModel.getReturnIntent().observe(this, Observer { onSuccessfulLogin(it!!) })
    }

    private fun showProgress(showProgress : Boolean?) {
        login_progress.visibility = if (showProgress!!) View.VISIBLE else View.GONE
        login_form.visibility = if (showProgress) View.GONE else View.VISIBLE
    }

    private fun onSuccessfulLogin(intent: Intent) {
        setAccountAuthenticatorResult(intent.extras!!)
        setResult(RESULT_OK, intent)

        finish()
        HomeActivity.goToHome(this)
    }
}
