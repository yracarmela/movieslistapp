package com.ycjg.movieslistapp.views.movies

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.data.models.list.MovieResponseList
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import com.ycjg.movieslistapp.utils.AppConstants
import javax.inject.Inject

class MoviesViewModel @Inject constructor(private val mMovieRepository: MovieRepository,
                                          private val schedulerProvider: SchedulerProvider): BaseViewModel() {

    private val TAG = "MoviesViewModel"

    private val mMovieList = MutableLiveData<List<MovieDetails>>()

    fun loadTrendingMovies(page: Int) {
        val mediaType = "movie"
        val timeWindow = "day"

        compositeDisposable.add(
            mMovieRepository.getTrendingList(mediaType, timeWindow, AppConstants.API_KEY, page)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe(this::getMoviesList
                ) { th: Throwable? -> onErrorResult(TAG, "loadTrendingMovies()", th)})
    }

    private fun getMoviesList(movieResponseList: MovieResponseList) {
        mMovieList.value = movieResponseList.results
    }

    fun getMovieList() : LiveData<List<MovieDetails>> {
        return mMovieList
    }
}
