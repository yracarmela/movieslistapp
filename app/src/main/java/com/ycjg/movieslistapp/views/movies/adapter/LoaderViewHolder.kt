package com.ycjg.movieslistapp.views.movies.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import kotlinx.android.synthetic.main.item_loader_layout.view.*

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
class LoaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var mProgressBar: ProgressBar? = itemView.progressbar

    init {
    }
}