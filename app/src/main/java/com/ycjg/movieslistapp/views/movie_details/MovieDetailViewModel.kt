package com.ycjg.movieslistapp.views.movie_details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.models.MovieDetails
import com.ycjg.movieslistapp.data.models.MovieReviews
import com.ycjg.movieslistapp.data.models.StatusResponse
import com.ycjg.movieslistapp.data.models.list.MovieReviewList
import com.ycjg.movieslistapp.data.prefs.PreferencesProvider
import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import com.ycjg.movieslistapp.utils.AppConstants
import javax.inject.Inject

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
class MovieDetailViewModel @Inject constructor(private val mMovieRepository: MovieRepository,
                                               private val schedulerProvider: SchedulerProvider,
                                               private val mPrefsProvider: PreferencesProvider) : BaseViewModel() {

    private val TAG = MovieDetailViewModel::class.java.simpleName
    private val movieDetails = MutableLiveData<MovieDetails>()
    private val movieReviews = MutableLiveData<List<MovieReviews>>()
    private val ratingStatus = MutableLiveData<StatusResponse>()
    private val removedRatingStatus = MutableLiveData<StatusResponse>()
    private val watchlistStatus = MutableLiveData<StatusResponse>()

    fun getMovieDetails(movieId: Long?) {
        compositeDisposable.add(mMovieRepository.getMovieDetails(movieId!!, AppConstants.API_KEY)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe( { details: MovieDetails ->
                movieDetails.value = details
            }){ th: Throwable? -> onErrorResult(TAG, "getMovieDetails()", th)})
    }

    fun getMovieReviews(movieId: Long?) {
        compositeDisposable.add(mMovieRepository.getMovieReviews(movieId!!, AppConstants.API_KEY)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe( { movieReviewList: MovieReviewList? ->
                movieReviews.value = movieReviewList?.results
            }) { th: Throwable? -> onErrorResult(TAG, "getMovieReviews()", th)})
    }

    fun rateMovie(movieId: Long?, rating: Int) {
        val sessionId = mPrefsProvider.getSessionId()
        compositeDisposable.add(
            mMovieRepository.rateMovie(movieId!!, AppConstants.API_KEY, sessionId!!, rating)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe( {status: StatusResponse? ->
                ratingStatus.value = status
            }) { th: Throwable? -> onErrorResult(TAG, "rateMovie()", th)})
    }

    fun removeRating(movieId: Long?) {
        val sessionId = mPrefsProvider.getSessionId()
        compositeDisposable.add(
            mMovieRepository.removeRating(movieId!!, AppConstants.API_KEY, sessionId!!)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe( {status: StatusResponse? ->
                    removedRatingStatus.value = status
                }) { th: Throwable? -> onErrorResult(TAG, "rateMovie()", th)})
    }

    fun addMovieToWatchlist(mediaId: Long) {
        val accountId = mPrefsProvider.getAccountId()
        val sessionId = mPrefsProvider.getSessionId()
        val mediaType = "movie"
        val isWatchlist = true
        compositeDisposable.add(
            mMovieRepository.addToWatchlist(accountId!!, AppConstants.API_KEY,
                sessionId!!, mediaType, mediaId, isWatchlist)
            .subscribeOn(schedulerProvider.io())
            .observeOn(schedulerProvider.ui())
            .subscribe( {status: StatusResponse? ->
                watchlistStatus.value = status
            }) { th: Throwable? -> onErrorResult(TAG, "addMovieToWatchlist()", th)})
    }

    fun getMovie(): LiveData<MovieDetails> {
        return movieDetails
    }

    fun getReviews(): LiveData<List<MovieReviews>> {
        return movieReviews
    }

    fun getRatingStatus(): LiveData<StatusResponse> {
        return ratingStatus
    }

    fun getRemovedRatingStatus(): LiveData<StatusResponse> {
        return removedRatingStatus
    }

    fun getWatchlistStatus(): LiveData<StatusResponse> {
        return watchlistStatus
    }
}