package com.ycjg.movieslistapp.views.home

import android.app.Activity
import com.ycjg.movieslistapp.common.BaseViewModel
import com.ycjg.movieslistapp.data.account_manager.AppAccountManagerHelper
import javax.inject.Inject

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
class HomeViewModel @Inject constructor(private val mManagerHelper: AppAccountManagerHelper) : BaseViewModel() {

    fun authenticateActivity(activity: Activity) {
        mManagerHelper.authenticateActivity(activity)
    }
}