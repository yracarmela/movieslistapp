package com.ycjg.movieslistapp.views.home

import android.app.Activity
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.CoordinatorLayout
import android.support.v4.app.Fragment
import com.ycjg.movieslistapp.R
import com.ycjg.movieslistapp.common.BaseActivity
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.utils.BottomNavigationBehavior
import com.ycjg.movieslistapp.views.watchlist.WatchlistFragment
import com.ycjg.movieslistapp.views.movies.MoviesFragment
import com.ycjg.movieslistapp.views.search.SearchFragment
import kotlinx.android.synthetic.main.activity_home.*
import javax.inject.Inject

class HomeActivity : BaseActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var homeViewModel: HomeViewModel

    companion object {

        fun getStartIntent(context: Context): Intent {
            return Intent(context, HomeActivity::class.java)
        }

        fun goToHome(context: Context) {
            goToHome(context, false)
        }

        fun goToHome(context: Context, isExiting: Boolean) {
            val intent = Intent(context, HomeActivity::class.java)
            intent.putExtra(AppConstants.EXITING_TAG, isExiting)

            if (context !is Activity) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

            context.startActivity(intent)
        }
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                title = resources.getString(R.string.title_home)
                loadFragment(MoviesFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_search -> {
                title = resources.getString(R.string.search)
                loadFragment(SearchFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_list -> {
                title = resources.getString(R.string.my_list)
                loadFragment(WatchlistFragment.newInstance())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        homeViewModel = ViewModelProviders.of(this, viewModelFactory)
            .get(HomeViewModel::class.java)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        val layoutParams: CoordinatorLayout.LayoutParams = navigation.layoutParams as CoordinatorLayout.LayoutParams
        layoutParams.behavior = BottomNavigationBehavior()
        setUp()
    }

    override fun setUp() {
        val isExiting = intent.getBooleanExtra(AppConstants.EXITING_TAG, false)
        if (isExiting)
            finish()
        else
            homeViewModel.authenticateActivity(this)

        loadFragment(MoviesFragment.newInstance())
    }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }
}
