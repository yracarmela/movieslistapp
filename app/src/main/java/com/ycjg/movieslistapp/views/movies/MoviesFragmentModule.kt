package com.ycjg.movieslistapp.views.movies

import com.ycjg.movieslistapp.data.repository.MovieRepository
import com.ycjg.movieslistapp.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
@Module
class MoviesFragmentModule {

    @Provides
    fun provideViewModel(mMoviesRepository: MovieRepository, schedulerProvider: SchedulerProvider)
            = MoviesViewModel(mMoviesRepository, schedulerProvider)
}