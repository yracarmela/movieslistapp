package com.ycjg.movieslistapp.common

import com.ycjg.movieslistapp.auth.AccountAuthenticatorActivity

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
abstract class BaseAuthActivity : AccountAuthenticatorActivity()