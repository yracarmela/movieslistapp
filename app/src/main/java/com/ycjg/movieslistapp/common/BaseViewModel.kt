package com.ycjg.movieslistapp.common

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.ycjg.movieslistapp.data.models.ErrorData
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
open class BaseViewModel : ViewModel() {

    val loader : MutableLiveData<Boolean> = MutableLiveData()
    val error : MutableLiveData<ErrorData?> = MutableLiveData()
    var compositeDisposable : CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun onErrorResult(callingClass: String, method: String, throwable: Throwable?) {
        Log.e(callingClass, "", throwable)
    }
}