package com.ycjg.movieslistapp.common

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.ycjg.movieslistapp.R
import dagger.android.support.DaggerAppCompatActivity

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
@SuppressLint("Registered")
abstract class BaseActivity : DaggerAppCompatActivity() {

    fun observerLoader(viewModel: BaseViewModel, loaderView : View) {
        viewModel.loader.observe(this, Observer {
            loaderView.visibility = if(it != null && it) View.VISIBLE else View.GONE
        })
    }

    fun onError(@StringRes message : Int) {
        onError(resources.getString(message))
    }

    fun onError(message : String) {
        createSnackBar(message)
    }

    private fun createSnackBar(message : String) {
        val snackBar : Snackbar = Snackbar.make(findViewById(android.R.id.content),
            message, Snackbar.LENGTH_SHORT)
        val view : View = snackBar.view
        val textView : TextView = view.findViewById(android.support.design.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackBar.show()
    }

    fun showMessage(@StringRes message : Int) {
        showMessage(resources.getString(message))
    }

    fun showMessage(message : String) {
        if (message != null)
            createToast(message)
        else
            createToast(resources.getString(R.string.some_error))
    }

    private fun createToast(message : String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    abstract fun setUp()
}