package com.ycjg.movieslistapp.common

import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.ycjg.movieslistapp.R
import dagger.android.support.DaggerFragment

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
abstract class BaseFragment : DaggerFragment() {
    fun onError(@StringRes message : Int) {
        onError(resources.getString(message))
    }

    fun onError(message : String) {
        createSnackBar(message)
    }

    private fun createSnackBar(message : String) {
        val snackBar : Snackbar = Snackbar.make(
            activity!!.findViewById(android.R.id.content),
            message, Snackbar.LENGTH_SHORT)
        val view : View = snackBar.view
        val textView : TextView = view.findViewById(android.support.design.R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(context!!, R.color.white))
        snackBar.show()
    }

    fun showMessage(@StringRes message : Int) {
        showMessage(resources.getString(message))
    }

    fun showMessage(message : String) {
        if (message != null)
            createToast(message)
        else
            createToast(resources.getString(R.string.some_error))
    }

    private fun createToast(message : String) {
        Toast.makeText(context!!, message, Toast.LENGTH_SHORT).show()
    }

    abstract fun setUp()
}