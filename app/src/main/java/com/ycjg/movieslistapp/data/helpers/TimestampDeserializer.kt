package com.ycjg.movieslistapp.data.helpers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
class TimestampDeserializer : JsonDeserializer<Timestamp>() {

    override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Timestamp? {
        val oc: ObjectCodec? = p?.codec
        val node: JsonNode? = oc?.readTree(p)
        val dateStr: String? = node?.asText()

        val date: java.util.Date? = SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.getDefault()).parse(dateStr)
        return date?.time?.let { Timestamp(it) }
    }

}