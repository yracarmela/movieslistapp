package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class StatusResponse {

    @JsonProperty("status_code")
    var statusCode: Int = 0

    @JsonProperty("status_message")
    var statusMessage: String = ""
}