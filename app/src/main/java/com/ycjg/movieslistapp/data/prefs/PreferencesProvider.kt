package com.ycjg.movieslistapp.data.prefs

import android.content.Context
import android.content.SharedPreferences
import com.ycjg.movieslistapp.utils.AppConstants
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
@Singleton
class PreferencesProvider @Inject constructor(var mContext: Context) {
    private val IS_FIRST_LOGIN = "/pref_key_first_login"
    private val CURRENT_USER = "pref_key_current_user"
    private val SESSION_ID = "pref_key_session_id"
    private val ACCOUNT_ID = "/pref_key_account_id"
    private val ACCOUNT_NAME = "/pref_key_account_name"

    private var mPrefs: SharedPreferences = mContext.getSharedPreferences(AppConstants.PREFS_NAME, Context.MODE_PRIVATE)

    fun setFirstLogin(isFirstLogin: Boolean) {
        mPrefs.edit().putBoolean(getKeyPrefix(IS_FIRST_LOGIN), isFirstLogin).apply()
    }

    fun getFirstLogin() : Boolean {
        return mPrefs.getBoolean(getKeyPrefix(IS_FIRST_LOGIN), AppConstants.DEFAULT_BOOLEAN)
    }

    fun setCurrentUser(userId: String) {
        mPrefs.edit().putString(CURRENT_USER, userId).apply()
    }

    fun getCurrentUser(): String? {
        return mPrefs.getString(CURRENT_USER, AppConstants.DEFAULT_STRING)
    }

    fun setSessionId(sessionId: String) {
        mPrefs.edit().putString(SESSION_ID, sessionId).apply()
    }

    fun getSessionId() : String? {
        return mPrefs.getString(SESSION_ID, AppConstants.DEFAULT_STRING)
    }

    fun setAccountId(accountId: Long) {
        mPrefs.edit().putLong(getKeyPrefix(ACCOUNT_ID), accountId).apply()
    }

    fun getAccountId(): Long? {
        return mPrefs.getLong(getKeyPrefix(ACCOUNT_ID), AppConstants.DEFAULT_LONG)
    }

    fun setAccountName(accountName: String) {
        mPrefs.edit().putString(getKeyPrefix(ACCOUNT_NAME), accountName).apply()
    }

    fun getAccountName(): String? {
        return mPrefs.getString(getKeyPrefix(ACCOUNT_NAME), AppConstants.DEFAULT_STRING)
    }

    private fun getKeyPrefix(code: String): String {
        val userId = getCurrentUser()
        return "userId-$userId$code"
    }
}