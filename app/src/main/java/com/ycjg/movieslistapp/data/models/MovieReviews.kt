package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class MovieReviews {

    @JsonProperty("author")
    var author: String? = ""

    @JsonProperty("content")
    var reviewContent: String? = ""

    @JsonProperty("id")
    var reviewId: String = ""

    @JsonProperty("url")
    var reviewUrl: String = ""
}