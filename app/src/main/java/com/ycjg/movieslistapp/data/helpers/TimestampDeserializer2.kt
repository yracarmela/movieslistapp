package com.ycjg.movieslistapp.data.helpers

import android.util.Log
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.ObjectCodec
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.JsonNode
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
class TimestampDeserializer2 : JsonDeserializer<Timestamp>() {

        override fun deserialize(p: JsonParser?, ctxt: DeserializationContext?): Timestamp? {
            val oc: ObjectCodec? = p?.codec
            val node: JsonNode? = oc?.readTree(p)
            val dateStr: String? = node?.asText()

            try {
                val date: Date?
                return if (dateStr!!.isNotEmpty()) {
                    date = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(dateStr)
                    date?.time?.let { Timestamp(it) }
                } else null

            } catch (e: JsonMappingException) {
                Log.e(TimestampDeserializer2::class.java.simpleName, "deserialize()", e)
            }
            return null
        }
}