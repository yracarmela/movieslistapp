package com.ycjg.movieslistapp.data.repository

import com.ycjg.movieslistapp.data.MovieService
import com.ycjg.movieslistapp.data.models.*
import com.ycjg.movieslistapp.data.models.list.MovieResponseList
import com.ycjg.movieslistapp.data.models.list.MovieReviewList
import com.ycjg.movieslistapp.data.models.list.MovieWatchlist
import com.ycjg.movieslistapp.di.annotations.ApplicationScope
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by yragalvez on 26/02/2019.
 * MoviesListApp
 */
@ApplicationScope
class MovieRepository @Inject constructor(private val movieService: MovieService) {

    fun createRequestToken(apiKey: String) : Observable<TokenResponse>
            = movieService.createNewToken(apiKey)

    fun createSessionId(apiKey: String, requestToken: String) : Observable<SessionResponse>
            = movieService.createSession(apiKey, requestToken)

    fun validateLogin(apiKey: String, userName: String, password: String, requestToken: String) :
            Observable<TokenResponse> = movieService.validateLogin(apiKey, userName, password, requestToken)

    fun getTrendingList(mediaType: String, timeWindow: String, apiKey: String, page: Int) :
            Observable<MovieResponseList> = movieService.getTrendingList(mediaType, timeWindow, apiKey, page)

    fun getMovieDetails(movieId: Long, apiKey: String) : Observable<MovieDetails>
            = movieService.getMovieDetails(movieId, apiKey)

    fun getMovieReviews(movieId: Long, apiKey: String) : Observable<MovieReviewList>
            = movieService.getMovieReviews(movieId, apiKey)

    fun rateMovie(movieId: Long, apiKey: String, sessionId: String, rating: Int) : Observable<StatusResponse>
            = movieService.rateMovie(movieId, apiKey, sessionId, rating)

    fun removeRating(movieId: Long, apiKey: String, sessionId: String) : Observable<StatusResponse>
            = movieService.removeRating(movieId, apiKey, sessionId)

    fun addToWatchlist(accountId: Long, apiKey: String, sessionId: String, mediaType: String,
                       mediaId: Long, isWatchlist: Boolean) : Observable<StatusResponse>
            = movieService.addToWatchlist(accountId, apiKey, sessionId, mediaType, mediaId, isWatchlist)

    fun getAccountDetails(apiKey: String, sessionId: String) : Observable<AccountDetails>
            = movieService.getAccountDetails(apiKey, sessionId)

    fun getWatchlist(accountId: Long, apiKey: String, sessionId: String, page: Int) : Observable<MovieWatchlist>
            = movieService.getWatchlist(accountId, apiKey, sessionId, page)

    fun getSearchResult(apiKey: String, queryString: String, page: Int) : Observable<MovieResponseList>
            = movieService.getSearchResult(apiKey, queryString, page)
}