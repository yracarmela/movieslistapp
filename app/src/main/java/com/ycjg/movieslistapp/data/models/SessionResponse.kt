package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by yragalvez on 28/02/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class SessionResponse {
    @JsonProperty("success")
    var isSuccess: Boolean = true

    @JsonProperty("session_id")
    var sessionId: String = ""
}