package com.ycjg.movieslistapp.data.models

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
class User constructor(username: String, mPassword: String, mRequestToken: String) {
    var userName: String = username
    var password: String = mPassword
    var requestToken: String = mRequestToken
}