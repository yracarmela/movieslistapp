package com.ycjg.movieslistapp.data.account_manager

import android.accounts.*
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.ycjg.movieslistapp.data.helpers.DbHelper
import com.ycjg.movieslistapp.data.models.User
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.views.home.HomeActivity
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
@Singleton
class AppAccountManagerHelper @Inject constructor(private val mContext : Context) {
    private val TAG = "AppAccountManagerHelper"
    private val mAccountManager : AccountManager = AccountManager.get(mContext)
    private var mAccount : Account? = null
    private var mRequestToken: String = ""

    init {
        loadAndroidAccount()
    }

    private fun loadAndroidAccount() : Account? {
        var availableAccounts: Array<Account> = mAccountManager.getAccountsByType(AppConstants.ACCOUNT_TYPE)
        if (availableAccounts.isNotEmpty())
            mAccount = availableAccounts[0]
        return mAccount
    }

    fun isLoggedIn(activity : Activity) : Boolean {
        mAccount = loadAndroidAccount()
        return mAccount != null
    }

    fun getUserData() {
        //TODO: return User Data
    }

    fun authenticateActivity(activity: Activity) {
        if (!isLoggedIn(activity)) {
            loginActivity(activity)
            activity.finish()
        }
    }

    private fun loginActivity(activity: Activity) {
        mAccountManager.addAccount(AppConstants.ACCOUNT_TYPE, AppConstants.GENERAL_KEY_TYPE,
            null, null, activity, {accountManagerFuture: AccountManagerFuture<Bundle>? ->
                try {
                    accountManagerFuture?.result
                    HomeActivity.goToHome(activity)
                } catch (e : AuthenticatorException) {
                    Log.e(TAG, "loginActivity() AuthenticatorException", e)
                    navigateToMain(activity)
                } catch (e : OperationCanceledException) {
                    Log.e(TAG, "loginActivity() OperationCanceledException", e)
                    navigateToMain(activity)
                } catch (e : IOException) {
                    Log.e(TAG, "loginActivity()", e)
                }
            }, null)
    }

    private fun navigateToMain(activity: Activity) {
        HomeActivity.goToHome(activity, true)
        activity.finish()
    }

    fun addAccount(user: User, password: String ) : Intent {
        val userName = user.userName
        val requestToken = user.requestToken

        val account = Account(userName, AppConstants.ACCOUNT_TYPE)
        mAccountManager.addAccountExplicitly(account, null, null)
        mAccountManager.setUserData(account, AppConstants.USER_INFO_KEY, DbHelper.toJson(user))
        mAccountManager.setAuthToken(account, AppConstants.GENERAL_KEY_TYPE, requestToken)

        val intent = Intent()
        intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, userName)
        intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AppConstants.ACCOUNT_TYPE)
        intent.putExtra(AccountManager.KEY_AUTHTOKEN, requestToken)

        return intent
    }

    private fun removeFromAccountManager(activity: Activity) {
        loadAndroidAccount()

        mAccountManager.setUserData(mAccount, AppConstants.USER_INFO_KEY, null)
        mAccountManager.invalidateAuthToken(AppConstants.ACCOUNT_TYPE, mRequestToken)
        mAccountManager.removeAccount(mAccount, activity, {
            mAccount = null
            HomeActivity.goToHome(activity)
            activity.finish()
            Toast.makeText(mContext, "Successfully logged out", Toast.LENGTH_SHORT).show()
        }, null)

    }
}