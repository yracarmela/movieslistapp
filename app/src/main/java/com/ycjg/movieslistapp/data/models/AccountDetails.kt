package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class AccountDetails {

    @JsonProperty("id")
    var accountId: Long = 0L

    @JsonProperty("name")
    var accountName: String = ""

    @JsonProperty("include_adult")
    var includeAdult: Boolean = false

    @JsonProperty("username")
    var userName: String = ""
}