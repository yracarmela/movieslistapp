package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.ycjg.movieslistapp.data.helpers.TimestampDeserializer2
import java.sql.Timestamp

/**
 * Created by yragalvez on 02/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class MovieDetails {

    @JsonProperty("id")
    var movieId: Long = 0L

    @JsonProperty("adult")
    var isAdult: Boolean = false

    @JsonProperty("backdrop_path")
    var backdropPath: String? = ""

    @JsonProperty("genres")
    var genre: Array<Genre>? = null

    @JsonProperty("homepage")
    var homepage: String? = ""

    @JsonProperty("original_title")
    var originalTitle: String? = ""

    @JsonProperty("overview")
    var overview: String? = ""

    @JsonProperty("popularity")
    var popularity: Long = 0L

    @JsonProperty("poster_path")
    var posterPath: String? = ""

    @JsonProperty("release_date")
    @JsonDeserialize(using= TimestampDeserializer2::class)
    var releaseDate: Timestamp? = Timestamp(System.currentTimeMillis())

    @JsonProperty("runtime")
    var runtime: Long? = 0L

    @JsonProperty("status")
    var movieStatus: String? = ""

    @JsonProperty("tagline")
    var movieTagline: String? = ""

    @JsonProperty("title")
    var movieTitle: String = ""

    @JsonProperty("vote_average")
    var voteAverage: Int = 0

    @JsonProperty("vote_count")
    var voteCount: Long = 0L
}