package com.ycjg.movieslistapp.data

import com.ycjg.movieslistapp.data.models.*
import com.ycjg.movieslistapp.data.models.list.MovieResponseList
import com.ycjg.movieslistapp.data.models.list.MovieReviewList
import com.ycjg.movieslistapp.data.models.list.MovieWatchlist
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
interface MovieService {

    @GET("authentication/token/new")
    fun createNewToken(@Query("api_key") apiKey: String) : Observable<TokenResponse>

    @POST("authentication/session/new")
    @FormUrlEncoded
    fun createSession(@Query("api_key") apiKey: String, @Field("request_token") requestToken: String)
            : Observable<SessionResponse>

    @POST("authentication/token/validate_with_login")
    @FormUrlEncoded
    fun validateLogin(@Query("api_key") apiKey: String, @Field("username") userName: String,
                      @Field("password") password: String, @Field("request_token") requestToken: String)
            : Observable<TokenResponse>

    @GET("trending/{media_type}/{time_window}")
    fun getTrendingList(@Path("media_type") mediaType: String, @Path("time_window") timeWindow: String,
                        @Query("api_key") apiKey: String, @Query("page") page: Int)
            : Observable<MovieResponseList>

    @GET("movie/{movie_id}")
    fun getMovieDetails(@Path("movie_id") movieId: Long, @Query("api_key") apiKey: String)
            : Observable<MovieDetails>

    @GET("movie/{movie_id}/reviews")
    fun getMovieReviews(@Path("movie_id") movieId: Long, @Query("api_key") apiKey: String)
            : Observable<MovieReviewList>

    @POST("movie/{movie_id}/rating")
    @FormUrlEncoded
    fun rateMovie(@Path("movie_id") movieId: Long, @Query("api_key") apiKey: String,
                  @Query("session_id") sessionId: String, @Field("value") movieRating: Int) : Observable<StatusResponse>

    @DELETE("movie/{movie_id}/rating")
    fun removeRating(@Path("movie_id") movieId: Long, @Query("api_key") apiKey: String,
                     @Query("session_id") sessionId: String) : Observable<StatusResponse>

    @GET("account")
    fun getAccountDetails(@Query("api_key") apiKey: String, @Query("session_id") sessionId: String)
            : Observable<AccountDetails>

    @GET("account/{account_id}/watchlist/movies")
    fun getWatchlist(@Path("account_id") accountId: Long, @Query("api_key") apiKey: String,
                     @Query("session_id") sessionId: String, @Query("page") page: Int) : Observable<MovieWatchlist>

    @POST("account/{account_id}/watchlist")
    @FormUrlEncoded
    fun addToWatchlist(@Path("account_id") accountId: Long, @Query("api_key") apiKey: String,
                       @Query("session_id") sessionId: String, @Field("media_type") mediaType: String,
                       @Field("media_id") mediaId: Long, @Field("watchlist") isWatchList: Boolean) : Observable<StatusResponse>

    @GET("search/movie")
    fun getSearchResult(@Query("api_key") apiKey: String, @Query("query") queryString: String,
                        @Query("page") page: Int) : Observable<MovieResponseList>
}