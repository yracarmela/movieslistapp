package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.ycjg.movieslistapp.data.helpers.TimestampDeserializer
import java.sql.Timestamp

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class TokenResponse {
    @JsonProperty("success")
    var isSuccess: Boolean = true

    @JsonProperty("expires_at")
    @JsonDeserialize(using = TimestampDeserializer::class)
    var expiryDate: Timestamp = Timestamp(System.currentTimeMillis())

    @JsonProperty("request_token")
    var requestToken: String = ""
}