package com.ycjg.movieslistapp.data.models

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
class ErrorData(message : String = "", errorCode : String = "", throwable: Throwable? = null) {
    var message : String = message
    var errorCode : String = errorCode
    var throwable : Throwable? = throwable
}