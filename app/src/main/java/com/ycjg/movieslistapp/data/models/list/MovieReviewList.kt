package com.ycjg.movieslistapp.data.models.list

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.ycjg.movieslistapp.data.models.MovieReviews

/**
 * Created by yragalvez on 03/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class MovieReviewList {

    @JsonProperty("id")
    var movieId: Long = 0L

    @JsonProperty("page")
    var page: Int = 0

    @JsonProperty("results")
    var results: List<MovieReviews> = ArrayList()

    @JsonProperty("total_pages")
    var totalPages: Int = 0

    @JsonProperty("total_results")
    var totalResults: Int = 0
}