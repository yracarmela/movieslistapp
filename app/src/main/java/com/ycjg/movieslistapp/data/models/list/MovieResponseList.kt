package com.ycjg.movieslistapp.data.models.list

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.ycjg.movieslistapp.data.models.MovieDetails

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class MovieResponseList {

    @JsonProperty("page")
    var page: Int = 0

    @JsonProperty("results")
    var results: List<MovieDetails> = ArrayList()

    @JsonProperty("total_pages")
    var totalPages: Int = 0

    @JsonProperty("total_results")
    var totalResults: Int = 0
}