package com.ycjg.movieslistapp.data.helpers

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
class DbHelper {

    companion object {
        fun toJson(value: Any) : String? {
            val mapper = ObjectMapper()
            return try {
                mapper.writeValueAsString(value)
            } catch (e: JsonProcessingException) {
                e.printStackTrace()
                null
            }
        }
    }
}