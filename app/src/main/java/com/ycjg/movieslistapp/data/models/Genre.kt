package com.ycjg.movieslistapp.data.models

import com.fasterxml.jackson.annotation.JsonProperty

/**
 * Created by yragalvez on 01/03/2019.
 * MoviesListApp
 */
class Genre {
    @JsonProperty("id")
    var genreId: Int = 0

    @JsonProperty("name")
    var genreName: String = ""
}