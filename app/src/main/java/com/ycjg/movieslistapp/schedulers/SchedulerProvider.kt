package com.ycjg.movieslistapp.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
class SchedulerProvider {

    fun ui() : Scheduler {
        return AndroidSchedulers.mainThread()
    }

    fun io() : Scheduler {
        return Schedulers.io()
    }

    fun computation() : Scheduler {
        return Schedulers.computation()
    }
}