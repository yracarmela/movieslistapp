package com.ycjg.movieslistapp

import android.content.Context
import android.support.multidex.MultiDex
import com.ycjg.movieslistapp.di.component.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by yragalvez on 24/02/2019.
 * MoviesListApp
 */
class MoviesApp : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
    }

    override fun applicationInjector(): AndroidInjector<out MoviesApp> {
        return DaggerApplicationComponent.builder().create(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}