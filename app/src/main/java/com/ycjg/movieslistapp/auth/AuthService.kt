package com.ycjg.movieslistapp.auth

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.support.annotation.Nullable

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
class AuthService : Service() {

    @Nullable
    override fun onBind(p0: Intent?): IBinder? {
        return Authenticator(this).iBinder
    }
}