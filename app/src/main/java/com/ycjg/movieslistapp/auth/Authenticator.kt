package com.ycjg.movieslistapp.auth

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.accounts.AccountManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import com.ycjg.movieslistapp.utils.AppConstants
import com.ycjg.movieslistapp.views.login.LoginActivity

/**
 * Created by yragalvez on 27/02/2019.
 * MoviesListApp
 */
class Authenticator constructor(val mContext: Context) : AbstractAccountAuthenticator(mContext) {

    override fun confirmCredentials(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Bundle?): Bundle? {
        return null
    }

    override fun updateCredentials(accountAuthenticatorResponse: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, bundle: Bundle?): Bundle? {
        return null
    }

    override fun getAuthToken(accountAuthenticatorResponse: AccountAuthenticatorResponse?, account: Account?, authTokenType: String?, bundle: Bundle?): Bundle {
        if (!authTokenType.equals(AppConstants.GENERAL_KEY_TYPE)) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ERROR_MESSAGE, "Invalid authTokenType")
            return result
        }

        val am = AccountManager.get(mContext)
        val authToken = am.peekAuthToken(account, authTokenType)

        if (!TextUtils.isEmpty(authToken)) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account?.name)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account?.type)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }

        val intent = LoginActivity.getStartIntent(mContext)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, accountAuthenticatorResponse)
        val result = Bundle()
        result.putParcelable(AccountManager.KEY_INTENT, result)

        return result
    }

    override fun hasFeatures(p0: AccountAuthenticatorResponse?, p1: Account?, p2: Array<out String>?): Bundle {
        val result = Bundle()
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, false)
        return result
    }

    override fun editProperties(p0: AccountAuthenticatorResponse?, p1: String?): Bundle? {
        return null
    }

    override fun addAccount(
        p0: AccountAuthenticatorResponse?,
        p1: String?,
        p2: String?,
        p3: Array<out String>?,
        p4: Bundle?
    ): Bundle {
        val intent : Intent = LoginActivity.getStartIntent(mContext)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, p0)

        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

    override fun getAuthTokenLabel(p0: String?): String {
        return AppConstants.GENERAL_KEY_TYPE
    }
}